#!/usr/bin/bash

# make sure we have rt-setup installed
if rpm -q --quiet realtime-setup; then
    :
else
    cat <<EOF > /etc/yum.repos.d/centos-stream-rt.repo
[centos-stream-rt]
name=CentOS Stream latest Realtime compose
baseurl=http://composes.stream.centos.org/test/latest-CentOS-Stream/compose/RT/\$basearch/os/
enabled=1
gpgcheck=0
metadata_expire=6h
skip_if_unavailable=False
EOF
    sudo dnf install -y realtime-setup
    if [[ $? != 0 ]]; then
       echo "install of realtime-setup failed!"
       exit 1
    fi
fi

# check bins
for p in realtime-setup slub_cpu_partial_off; do
    prog=/usr/bin/$p
    if [[ ! -e $prog ]]; then
       echo "$prog not found"
       exit 2
    fi
    if [[ ! -x $prog ]]; then
       echo "$prog not executable!"
       exit 3
    fi
done

# check system bins
for p in kernel-is-rt realtime-entsk; do
    prog=/usr/sbin/$p
    if [[ ! -e $prog ]]; then
       echo "$prog not found"
       exit 4
    fi
    if [[ ! -x $prog ]]; then
       echo "$prog not executable!"
       exit 5
    fi
done

# check /etc files
if [[ ! -e /etc/security/limits.d/realtime.conf ]]; then
    echo "/etc/security/limits.d/realtime.conf: not found"
    exit 6
fi
if [[ ! -e /etc/sysconfig/realtime-setup ]]; then
    echo "/etc/sysconfig/realtime-setup: not found"
    exit 7
fi
if [[ ! -e /etc/udev/rules.d/99-rhel-rt.rules ]]; then
    echo "/etc/udev/rules.d/99-rhel-rt.rules: not found"
    exit 8
fi

# check systemd service files
for i in realtime-entsk realtime-setup; do
    serv="/usr/lib/systemd/system/$i.service"
    if [[ ! -e $serv ]]; then
       echo "$serv: not found"
       exit 9
    fi
done

exit 0
